package ai.maum.stats.producer.grpc

import ai.maum.stats.producer.kafka.MessageApiUsage
import ai.maum.stats.producer.kafka.MessageStatistics
import ai.maum.stats.producer.kafka.Producer
import ai.maum.stats.protobuf.*
import io.grpc.stub.StreamObserver
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.lognet.springboot.grpc.GRpcService
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import java.util.*

@Profile("!test")
@GRpcService
class StatsGrpcService(
        val producer: Producer
) : StatsGrpcServiceGrpc.StatsGrpcServiceImplBase() {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun saveApiUsage(request: SaveAPIUsageRequest, responseObserver: StreamObserver<SaveAPIUsageResponse>) {
        val topic = "ai.maum.stats.kafka.topic.api-usage"
        val uuid = UUID.randomUUID()

        try {
            logger.info("[$uuid] gRPC Call; rpc: saveApiUsage")

            val messageUsage = MessageApiUsage(
                    request.apiId,
                    request.elapsedTime,
                    request.engine,
                    request.service,
                    request.requestAosPackage,
                    request.requestIosUrlScheme,
                    request.requestApplicationName,
                    request.requestHost,
                    request.requestMethod,
                    request.requestPayloadSize,
                    request.requestTime,
                    request.requestContentType,
                    request.pathAbsolute,
                    request.responsePayloadSize,
                    request.responseStatusCode,
                    request.responseTime,
                    request.responseContentType,
                    request.success,
                    request.uuid
            )

            val jsonString = Json.encodeToString(messageUsage)

            logger.info("[$uuid] topic: $topic")
            logger.info("[$uuid] payload: $jsonString")

            producer.send(uuid, topic, jsonString)

            logger.info("[$uuid] kafka message sent successfully")

            val responseBuilder = SaveAPIUsageResponse.newBuilder()
                    .setMessage("success")
            responseObserver.onNext(responseBuilder.build())
            responseObserver.onCompleted()

            logger.info("[$uuid] gRPC responded successfully")
        } catch (e: Exception) {
            logger.error("[$uuid] ${e.message}")
            logger.debug("[$uuid] ${e.stackTraceToString()}")
        }
    }

    override fun saveStatistics(request: SaveStatisticsRequest, responseObserver: StreamObserver<SaveStatisticsResponse>) {
        val topic = "ai.maum.stats.kafka.topic.statistics"
        val uuid = UUID.randomUUID()

        try {
            logger.info("[$uuid] gRPC Call; rpc: saveApiUsage")

            val messageStatistics = MessageStatistics(
                    request.collection,
                    request.payload
            )

            val jsonString = Json.encodeToString(messageStatistics)

            logger.info("[$uuid] topic: $topic")
            logger.info("[$uuid] payload: $jsonString")

            producer.send(uuid, topic, jsonString)

            logger.info("[$uuid] kafka message sent successfully")

            val responseBuilder = SaveStatisticsResponse.newBuilder()
                    .setMessage("OK")
            responseObserver.onNext(responseBuilder.build())
            responseObserver.onCompleted()

            logger.info("[$uuid] gRPC responded successfully")
        } catch (e: Exception) {
            logger.error("[$uuid] ${e.message}")
            logger.debug("[$uuid] ${e.stackTraceToString()}")
        }
    }
}
