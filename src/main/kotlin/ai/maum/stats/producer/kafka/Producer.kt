package ai.maum.stats.producer.kafka

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct

@Profile("!test")
@Component
class Producer {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var producer: KafkaProducer<String, String>? = null

    @Value("\${spring.kafka.bootstrap-servers}")
    var bootstrapServer: String? = null

    @Value("\${spring.kafka.producer.key-serializer}")
    var keySerializer: String? = null

    @Value("\${spring.kafka.producer.value-serializer}")
    var valueSerializer: String? = null

    // Not used
    @Value("\${spring.kafka.template.default-topic}")
    var topicName: String? = null

    @PostConstruct
    fun build() {
        val properties = Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
        producer = KafkaProducer(properties);
    }

    fun send(uuid: UUID, topic: String, message: String) {
        val producerRecord = ProducerRecord<String, String>(topic, message)

        try {
            producer!!.send(producerRecord) { metadata, exception ->
                exception?.let {
                    logger.error("[$uuid] ${exception.message}")
                    logger.error("[$uuid] ${exception.stackTraceToString()}")
                }
            }
        } catch (e: Exception) {
            throw e
        } finally {
            producer?.flush()
        }
    }
}