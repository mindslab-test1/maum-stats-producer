package ai.maum.stats.producer.kafka

import kotlinx.serialization.Serializable

@Serializable
data class MessageStatistics (
        var collection: String,
        var payload: String
)