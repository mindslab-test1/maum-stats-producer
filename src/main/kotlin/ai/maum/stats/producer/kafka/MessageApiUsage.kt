package ai.maum.stats.producer.kafka

import kotlinx.serialization.Serializable

@Serializable
data class MessageApiUsage(
        var apiId: String,
        var elapsedTime: Long, //millis
        var engine: String,
        var service: String,
        var requestAosPackage: String,
        var requestIosUrlScheme: String,
        var requestApplicationName: String,
        var requestHost: String,
        var requestMethod: String,
        var requestPayloadSize: Long,
        var requestTime: Long,
        var requestContentType: String,
        var pathAbsolute: String,
        var responsePayloadSize: Long,
        var responseStatusCode: Long,
        var responseTime: Long,
        var responseContentType: String,
        var success: Boolean,
        var uuid: String,
)