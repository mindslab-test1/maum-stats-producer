package ai.maum.stats.producer

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("test")
class ProducerApplicationTests {
    @Test
    fun contextLoads() {

    }
}
